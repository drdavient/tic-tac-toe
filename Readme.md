# Tic Tac Toe
A Simple Tic Tac Toe Implementation in Unity using Unity Events to implement the observer pattern.
This is a simple test project to practise separation of concerns to write reusable code in C# for unity.
O and X pieces are reresented by sprites from [Blockships](http://playblockships.com)

## Video of Gameplay
[![](http://img.youtube.com/vi/09nc-zCQR1A/0.jpg)](http://www.youtube.com/watch?v=09nc-zCQR1A "Grass Victory Scene Video")
<img src="Screenshot_Grass.jpg" alt="Grass Screenshot" width="200"/>
![Image](ScreenShot.jpg)
## Implemented Features
* [x]  X Starts
* [x]  Player Randomly assigned X or O
* [x]  View PLaying Grid
* [x]  Player chooses position by clicking a tile
* [x]  AI places tiles at random
* [x]  Game end - Announces
    * [x]  “X Player Wins”
    * [x]  “O Player Wins”
    * [x]  “The Game is Tied”
    * [x]  Shows Quit Button

* [ ]  BG Graphics
* [ ]  BG Music

## Added XR Implementation
![Image](ScreenshotXR.jpg)
### Features
- [x]  XR View
   - [x]  Pose Turn on/off Grid
   - [x]  Select Grid Square
   - [x]  tap selected square to take turn
- [X]  Feel
   - [X]  FX when piece placed
   - [X]  FX when Win

### This project is created and  maintained by [dave@drdavient.com](mailto:dave@drdavient.com)

## Required Plugins / Packages
* Users of this project requires installation and a licence for [Odin Inspector](https://odininspector.com/)
* Users need to add TextMesh Pro from the Unity PackageManager.
* XR Requires [Feel](https://assetstore.unity.com/packages/tools/particles-effects/feel-183370) to handle juicy feedback

