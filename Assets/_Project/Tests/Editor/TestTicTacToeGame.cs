using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace com.drdavient.TicTacToe.Tests
{
    public class TestTicTacToeGame
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestTicTacToeReverseDiagonal()
        {
            // Assert.IsTrue(false);
            List<Vector2Int> diagonal = new List<Vector2Int>();
            for (int i = 0; i < 3; i++)
            {
                var cell = new Vector2Int(i, 2 - i);
                diagonal.Add(cell);
                Debug.Log(cell);
            }

        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator TestTicTacToeGameWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }

    }
}
