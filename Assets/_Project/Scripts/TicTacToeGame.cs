/*
/// Copyright Dr Davient Ltd 2022
/// http://drdavient.com
/// dave@drdavient.com
*/

using UnityEngine.Events;

namespace com.drdavient.TicTacToe
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using _Project.Scripts.Views;
    using Sirenix.Serialization;
    using UnityEditor;
    using UnityEngine;
    using Random = UnityEngine.Random;

    public class TicTacToeGame : MonoBehaviour
    {
        [SerializeField] private GameState _gameState;
        [SerializeField] private List<GridView> _gridViews;
        [SerializeField] private float _aiTurnTime = 2;

        private float _aiTimer;
        private Dictionary<Vector2Int, TileState> _openTiles;
        private Dictionary<Vector2Int, TileState> _usedTiles;
        private List<IEnumerable<TileState>> _winLines;


        private void Awake()
        {
            _gameState.Init();
            _openTiles = new Dictionary<Vector2Int, TileState>();
            _usedTiles = new Dictionary<Vector2Int, TileState>();

            for (var i = 0; i < _gameState.TileStates.Count; i++)
            {
                var tileState = _gameState.TileStates[i];
                tileState.State = OXState.Empty;
                _openTiles.Add(tileState.Position, tileState);
            }

        }

        private void OnValidate()
        {
            if (_gridViews == null) _gridViews = new List<GridView>();
        }

        private void Start()
        {
            for (int k = 0; k < _gridViews.Count; k++)
            {
                for (var i = 0; i < _gameState.TileStates.Count; i++)
                {
                    var tileState = _gameState.TileStates[i];
                    var tileView = _gridViews[k].TileViews[i];
                    tileView.RegisterClickHandler(() => OnTileCLick(tileState.Position));
                    tileState.TileStateChanged.AddListener(tileView.SetState);
                }
            }


            SetupWinLines();
        }

        private void Update()
        {
            if (_gameState.GameOver) return;
            if (!_gameState.IsPlayerTurn)
            {
                _aiTimer += Time.deltaTime;
                if (_aiTimer > _aiTurnTime)
                {
                    TakeAITurn();
                    _aiTimer = 0;
                }
            }
        }

        private void SetupWinLines()
        {
            _winLines = new List<IEnumerable<TileState>>();
            for (var i = 0; i < 3; i++)
            {
                _winLines.Add(UsedTilesInColumn(i));
                _winLines.Add(UsedTilesInRow(i));
            }

            _winLines.Add(UsedTilesInPositiveDiagonal());
            _winLines.Add(UsedTilesInNegativeDiagonal());
        }


        public void OnTileCLick(Vector2Int position)
        {
            if (_gameState.IsPlayerTurn)
            {
                TakeTurn(position);
                return;
            }
#if UNITY_EDITOR
            Debug.Log($"Not your turn {position}");
#endif
        }


        private void TakeTurn(Vector2Int position)
        {
            if (_gameState.GameOver) return;

            if (!TryToPlaceTile(position)) return;

            if (CheckForWin()) return;

            if (CheckForDraw()) return;

            _gameState.NextPlayer();
        }

        private bool TryToPlaceTile(Vector2Int position)
        {
            if (_openTiles.TryGetValue(position, out var tileState))
            {
                tileState.State = _gameState.CurrentTile;
                _usedTiles.Add(tileState.Position, tileState);
                _openTiles.Remove(tileState.Position);
                return true;
            }

            return false;
        }

        private bool CheckForWin()
        {
            foreach (var winLine in _winLines)
                if (IsAwinner(winLine))
                {
                    _gameState.SetWinner(winLine);
                    return true;
                }

            return false;
        }

        private IEnumerable<TileState> UsedTilesInColumn(int i)
        {
            return _usedTiles.Where(cell => cell.Key.y == i).Select(cell => cell.Value);
        }

        private IEnumerable<TileState> UsedTilesInRow(int i)
        {
            return _usedTiles.Where(cell => cell.Key.x == i).Select(cell => cell.Value);
        }

        private IEnumerable<TileState> UsedTilesInNegativeDiagonal()
        {
            return _usedTiles.Where(cell => cell.Key.x == 2 - cell.Key.y).Select(cell => cell.Value);
        }

        private IEnumerable<TileState> UsedTilesInPositiveDiagonal()
        {
            return _usedTiles.Where(cell => cell.Key.y == cell.Key.x).Select(cell => cell.Value);
        }


        private bool IsAwinner(IEnumerable<TileState> winLine)
        {
            if (winLine.Count() == 3 &&
                winLine.All(cell => cell.State == winLine.ElementAt(0).State))
                return true;

            return false;
        }


        private bool CheckForDraw()
        {
            if (_openTiles.Count == 0)
            {
                _gameState.SetWinner(null);
                return true;
            }

            return false;
        }

        private void TakeAITurn()
        {
            TakeTurn(_openTiles.ElementAt(Random.Range(0, _openTiles.Count)).Key);
        }

        public void Quit()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }
    }
}