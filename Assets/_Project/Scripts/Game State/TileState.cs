﻿using System;
using UnityEngine;
using UnityEngine.Events;

public enum OXState

{
    Empty = 0,
    X = 1,
    O = 2
}

[Serializable]
[CreateAssetMenu(fileName = "TileState", menuName = "ScriptableObjects/TileState", order = 1)]
public class TileState : ScriptableObject
{
    public TileStateChanged TileStateChanged;
    public Vector2Int Position;

    public OXState _state;

    public OXState State
    {
        get => _state;
        set
        {
            if (_state == value) return;

            _state = value;
            OnTileStateChanged();
        }
    }

    public static TileState Create(int row, int column)
    {
        var tileState = CreateInstance(typeof(TileState)) as TileState;
        tileState.TileStateChanged = new TileStateChanged();
        tileState.name = $"Grid_{row}_{column}";
        tileState.Position = new Vector2Int(row, column);
        return tileState;
    }

    private void OnTileStateChanged()
    {
        TileStateChanged?.Invoke((int)State);
    }
}

[Serializable]
public class TileStateChanged : UnityEvent<int>
{
}