/*
/// Copyright Dr Davient Ltd 2022
/// http://drdavient.com
/// dave@drdavient.com
*/

using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "GameState", menuName = "ScriptableObjects/GameState", order = 1)]
public class GameState : SerializedScriptableObject
{
    public GameStateChanged GameStateChanged;

    //public TileState TileStateAsset;
    public List<TileState> TileStates;
    public bool IsXTurn = true;
    public bool IsPlayerX; // => CPU is O
    public bool GameOver;
    [OdinSerialize]  public OXState Winner = OXState.Empty;
    public List<TileState> WinLine;

    public Vector2 GridSize;
    private string _gameState;
    public bool IsPlayerTurn => (IsXTurn && IsPlayerX) || (!IsXTurn && !IsPlayerX);
    public OXState CurrentTile => IsXTurn ? OXState.X : OXState.O;

    public string GameStateString
    {
        get => _gameState;
        set
        {
            if (_gameState == value) return;

            _gameState = value;
            OnGameStateChanged();
        }
    }

    public void Init()
    {
        IsXTurn = true;
        RandomiseXPlayer();
        SetupGrid();
        SetGameStateText();
    }

    public void RandomiseXPlayer()
    {
        IsPlayerX = Random.Range(0, 2) == 1;
    }

    public void NextPlayer()
    {
        IsXTurn = !IsXTurn;
        SetGameStateText();
    }

    public void SetGameStateText()
    {
        GameStateString = IsPlayerTurn ? $"Player\n{CurrentTile} Turn" : $"CPU\n{CurrentTile} Turn";
    }

    private void OnGameStateChanged()
    {
        GameStateChanged?.Invoke(GameOver, _gameState);
    }

    [Button]
    public void SetupGrid()
    {
        GameOver = false;
        Winner = OXState.Empty;
        IsXTurn = true;
        TileStates = new List<TileState>();
        for (var row = 0; row < GridSize.x; row++)
        for (var column = 0; column < GridSize.y; column++)
            TileStates.Add(TileState.Create(row, column));
    }


    public void SetWinner(IEnumerable<TileState> winLine)
    {
        if (GameOver) return;
        GameOver = true;

        List<TileState> tileStates = winLine?.ToList();
        if (winLine == null)
        {
            Winner = OXState.Empty;
        }
        else
        {
            Winner = tileStates.ElementAt(0).State;
        }
        
        GameStateString = Winner != OXState.Empty ? $"Game Over:\n{Winner} Wins" : "Game Over:\n Draw";

        foreach (var tile in tileStates ?? Enumerable.Empty<TileState>()) tile.TileStateChanged.Invoke(3); // todo clean up magic numbers!
#if UNITY_EDITOR
        Debug.Log($"Winner {Winner}");
#endif
    }
}


[Serializable]
public class GameStateChanged : UnityEvent<bool, string>
{
}