/*
/// Copyright Dr Davient Ltd 2022
/// http://drdavient.com
/// dave@drdavient.com
*/

using _Project.Scripts.Views;
using MoreMountains.Feedbacks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace com.drdavient.TicTacToe.XR
{
    public class XRTileView : BaseView
    {
        [SerializeField] public UnityEvent PiecePlaced;
        [SerializeField] private MeshRenderer _meshRenderer;
        private readonly float _clickTimeLimit = 0.25f;
        private float _clickTimer;
        private GameObject _piece;
        private XRGridManager _xrGridManager;

        private void Awake()
        {
            _xrGridManager = FindObjectOfType<XRGridManager>(); // Quick and dirty
            _meshRenderer = GetComponent<MeshRenderer>();
        }

        private void Update()
        {
            if (_clickTimer < _clickTimeLimit)
            {
                _clickTimer += Time.deltaTime;
                _meshRenderer.material.color = Color.Lerp(_xrGridManager.SelectedColour,
                    _xrGridManager.UnSelectedColour, _clickTimer / _clickTimeLimit);
            }
        }

        public override void SetState(int state)
        {
            if(Application.isEditor) return;
            
            switch (state)
            {
                case 0:
                    if (_piece != null) Destroy(_piece);
                    break;
                case 1:
                    PlacePiece(_xrGridManager.XPiece);
                    break;
                case 2:
                    PlacePiece(_xrGridManager.OPiece);
                    break;
                case 3:
                    _piece.GetComponent<MMF_Player>()?.PlayFeedbacks();
                    break;
            }
        }

        // Quick and dirty implementation destroying/creating GameObjects
        // In a larger piece would manage this differently
        // Either using local caching or an object pool 
        private void PlacePiece(GameObject piece)
        {
            if (_piece != null) Destroy(_piece);
            _piece = Instantiate(piece, transform);
            _piece.GetComponent<MMF_Player>()?.PlayFeedbacks();
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            _clickTimer = 0;
            base.OnPointerClick(eventData);
        }
    }
}