/*
/// Copyright Dr Davient Ltd 2022
/// http://drdavient.com
/// dave@drdavient.com
*/

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

namespace com.drdavient.TicTacToe.XR
{
    public class XRGridManager : MonoBehaviour
    {
        [SerializeField] private GameObject _xrTicTacToeGrid;
        [SerializeField] private GameObject _placementIndicator;
        [SerializeField] public GameObject OPiece;
        [SerializeField] public GameObject XPiece;

        [SerializeField] private Color _selectedColour;
        [SerializeField] private Color _unSelectedColour;
        [SerializeField] private float _touchTimeoutLimit;

        private Camera _xrCamera;

        private  Vector3 _outOfView = new Vector3(1000f, 1000f, 1000f);
        private ARRaycastManager _arRaycastManager;
        private Pose _placementPose;


        private bool _placementPoseIsValid;
        private float _touchTimeout;

        public Color SelectedColour => _selectedColour;

        public Color UnSelectedColour => _unSelectedColour;


        private void Awake()
        {
            //  _xrTicTacToeGrid.gameObject.SetActive(false);
            _arRaycastManager = FindObjectOfType<ARRaycastManager>(); // Quick and dirty
            EnhancedTouchSupport.Enable();
            _xrCamera = FindObjectOfType<ARSessionOrigin>().camera;
        }


        private void Update()
        {
            if (Application.isEditor) return;
            UpdatePlacementPose(); // Where can we put our tic tac toe grid? 
            UpdatePlacementIndicator(); // move the indicator to follow
            HandleTouchInput(); // Interact with TicTacToe Game
        }


        private void HandleTouchInput()
        {
            _touchTimeout -= Time.deltaTime;

           
            if (!_placementPoseIsValid || Touch.activeFingers.Count != 1 ||
                !Touch.activeFingers[0].currentTouch.began) return;

            if (!(_touchTimeout < 0)) return;

            _touchTimeout = _touchTimeoutLimit;
           // if (IsTouching(out var canvasHit, "CanvasGrid") ) return;
            
            if (IsTouching( out  var tileHit,"TicTacToeTile"))
            {
                tileHit.collider.gameObject.GetComponent<XRTileView>().OnPointerClick(null);
                return;
            }
            
            
            if (!_showPlacementIndicator)
            {
                EnableGrid(false);
                return;
            }
            EnableGrid(true);
           
        }
        
        

        private bool IsTouching(out RaycastHit hit, string tag)
        {
            var touch = Touch.activeFingers[0].currentTouch;
            
            if (PointerOverUI.IsPointerOverUIObject(touch.screenPosition))
            {
                hit = new RaycastHit();
                return false;
            }

            var ray = _xrCamera.ScreenPointToRay(touch.screenPosition);

            if (Physics.Raycast(ray, out hit))
                if (hit.collider.CompareTag(tag))
                    return true;

            return false;
        }
        
        // private static bool IsTouchingPlacementIndicator(out RaycastHit hit)
        // {
        //     var touch = Touch.activeFingers[0].currentTouch;
        //     var ray = Camera.main.ScreenPointToRay(touch.screenPosition);
        //
        //
        //     if (Physics.Raycast(ray, out hit))
        //         if (hit.collider.CompareTag("PlacementIndicator"))
        //             return true;
        //
        //     return false;
        // }

        private void EnableGrid(bool enable)
        {
            // _xrTicTacToeGrid.gameObject.SetActive(enable);
            // if (!enable) return;
            //
            // _xrTicTacToeGrid.transform.SetPositionAndRotation(_placementPose.position,
            //     _placementPose.rotation);
            // _xrTicTacToeGrid.gameObject.SetActive(enable);
            _showPlacementIndicator = !enable;
            if (enable)
            {
               
                _xrTicTacToeGrid.transform.SetPositionAndRotation(_placementPose.position,
                    _placementPose.rotation);
            }
            else
            {
                _xrTicTacToeGrid.transform.SetPositionAndRotation(_outOfView, 
                    Quaternion.identity);
            }
        }

        private bool _showPlacementIndicator = true;

        private void UpdatePlacementIndicator()
        {
            var valid = _placementPoseIsValid && _showPlacementIndicator;
            _placementIndicator.SetActive(valid);
            if (valid)
                _placementIndicator.transform.SetPositionAndRotation(_placementPose.position, _placementPose.rotation);
        }

        private void UpdatePlacementPose()
        {
            var screenCenter = _xrCamera.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
            var hits = new List<ARRaycastHit>();

            _placementPoseIsValid = _arRaycastManager.Raycast(screenCenter, hits, TrackableType.Planes);

            if (_placementPoseIsValid) _placementPose = hits[0].pose;
        }
    }
    
    public class PointerOverUI
    {
        public static bool IsPointerOverUIObject(Vector2 touchPosition)
        {
            PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
            pointerEventData.position = touchPosition;
            List<RaycastResult> raycastResults = new List<RaycastResult>();
 
            EventSystem.current.RaycastAll(pointerEventData, raycastResults);
 
            return raycastResults.Count > 0;
        }
    }
}