/*
/// Copyright Dr Davient Ltd 2022
/// http://drdavient.com
/// dave@drdavient.com
*/

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatusView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _status;
    [SerializeField] private GameState _gameState;
    [SerializeField] private Button _quitButton;

    private void Awake()
    {
        _gameState.GameStateChanged.AddListener(OnStateChange);
        _quitButton.gameObject.SetActive(false);
    }

    public void OnStateChange(bool gameOver, string state)
    {
        _status.text = state;
        _quitButton.gameObject.SetActive(gameOver);
    }
}