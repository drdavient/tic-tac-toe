﻿using UnityEngine.Events;

namespace _Project.Scripts.Views
{
    public interface ITileView
    {
        void RegisterClickHandler(UnityAction action);

        void SetState(int state);
    }
}