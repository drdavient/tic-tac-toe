/*
/// Copyright Dr Davient Ltd 2022
/// http://drdavient.com
/// dave@drdavient.com
*/

using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Project.Scripts.Views
{
    [RequireComponent(typeof(Image))]
    public class TileView : BaseView, IPointerClickHandler
    {
        [SerializeField] private Sprite _empty;
        [SerializeField] private Sprite _x;
        [SerializeField] private Sprite _o;
        private readonly BaseView _baseView;


        private Image _image;

        
        private void Awake()
        {
            _image = GetComponent<Image>();
        }
        

        [Button]
        public override void SetState(int state)
        {
            switch (state)
            {
                case 0:
                    _image.sprite = _empty;
                    break;
                case 1:
                    _image.sprite = _x;
                    break;
                case 2:
                    _image.sprite = _o;
                    break;
            }
        }


        // [Button]
        // public override void OnPointerClick(PointerEventData eventData)
        // {
        //     _baseView.OnPointerClick(eventData);
        // }
    }
}