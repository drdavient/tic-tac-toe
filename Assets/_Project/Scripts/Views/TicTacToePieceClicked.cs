using System;
using UnityEngine.Events;

namespace _Project.Scripts.Views
{
    [Serializable]
    public class TicTacToePieceClicked : UnityEvent
    {
    }
}