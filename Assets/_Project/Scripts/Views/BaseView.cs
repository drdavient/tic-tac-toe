﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Views
{
    public abstract class BaseView : MonoBehaviour, ITileView
    {
        [SerializeField] private TicTacToePieceClicked _ticTacToePieceClicked;
        private TileView _tileView;


        public void RegisterClickHandler(UnityAction action)
        {
            _ticTacToePieceClicked.AddListener(action);
        }

        [Button]
        public abstract void SetState(int state);

        [Button]
        public virtual void OnPointerClick(PointerEventData eventData)
        {
#if UNITY_EDITOR
            Debug.Log("Tile Clicked");
#endif
            _ticTacToePieceClicked.Invoke();
        }
    }
}