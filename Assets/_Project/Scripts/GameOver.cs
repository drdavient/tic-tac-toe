/*
/// Copyright Dr Davient Ltd 2022
/// http://drdavient.com
/// dave@drdavient.com
*/

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace com.drdavient.TicTacToe
{
    public class GameOver : MonoBehaviour
    {
        private TextMeshPro _textMeshPro;
        [SerializeField] private GameObject[] _disableOnGameOver;
        [SerializeField] private GameObject[] _enableOnGameOver;
        void Awake()
        {
            _textMeshPro = GetComponent<TextMeshPro>();
            _gameState.GameStateChanged.AddListener(OnStateChange);
            gameObject.SetActive(false);
        }
        [SerializeField] private GameState _gameState;

        public void OnStateChange(bool gameOver, string state)
        {
            if (gameOver)
            {
                gameObject.SetActive(true);
                StartCoroutine(DoGameOver(state));
            }
               
        }

        private IEnumerator DoGameOver(string state)
        {
            yield return new WaitForSeconds(1);
            _textMeshPro.text = state;
            foreach (var objectToDisable in _disableOnGameOver)
            {
                objectToDisable.SetActive(false);
            }
            foreach (var objectToEnable in _enableOnGameOver)
            {
                objectToEnable.SetActive(true);
            }
            
        }
    }
}
